var casper = require('casper').create({
    viewportSize: {width: 1980, height: 1080}
});
casper.start('https://www.packtpub.com/packt/offers/free-learning', function() {
    this.echo(this.getTitle());
})
.then(function(){
    console.log('waiting');
    this.waitForSelector('.login-popup');
    // links.click();
    // this.waitForSelector('.account-bar-form-left');
    // document.getElementsById("email").text = 'rhinoandre@gmail.com';
    // console.log(document.getElementsById("email").text);

})
.thenClick('.login-popup', function(){
    console.log('Link clicked');
    this.waitForSelector('form[action="https://www.packtpub.com/packt/offers/free-learning"]');
})
.then(function(){
    console.log('Email displayed');
    this.fill('form#packt-user-login-form', {email: 'rhinoandre@gmail.com', password: 'rh1n02ndre'}, true);
    this.capture('FormFilled.png', {
        top: 0,
        left: 0,
        width: 1980,
        height: 1080
    });
    console.log(document.getElementById('edit-submit-1'));
})
.then(function () {
    console.log('Form sent');
    this.capture('FormSent.png', {
        top: 0,
        left: 0,
        width: 1980,
        height: 1080
    });
})
.run();
